> **Build:** docker build -t zippa/es-nginx-auth .

> **Run:** docker run -d --name es-nginx-auth-container1 -p 9200:9200 zippa/es-nginx-auth

> **Note:** Username:**es** Password: **es**

> **Ispired By:** [playing-http-tricks-nginx](https://www.elastic.co/blog/playing-http-tricks-nginx)
.